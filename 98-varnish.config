packages:
  yum:
    varnish: []

files:
  "/etc/varnish/default.vcl" :
    owner: root
    group: root
    content: |
      backend default {
          .host = "127.0.0.1";
          .port = "8080";
      }

      sub vcl_recv {

        # unless sessionid/csrftoken is in the request, don't pass ANY cookies (referral_source, utm, etc)
        if (req.request == "GET" && (req.url ~ "^/static" || (req.http.cookie !~ "sessionid" && req.http.cookie !~ "csrftoken"))) {
          remove req.http.Cookie;
        }

        # normalize accept-encoding to account for different browsers
        # see: https://www.varnish-cache.org/trac/wiki/VCLExampleNormalizeAcceptEncoding
        if (req.http.Accept-Encoding) {
          if (req.http.Accept-Encoding ~ "gzip") {
            set req.http.Accept-Encoding = "gzip";
          } elsif (req.http.Accept-Encoding ~ "deflate") {
            set req.http.Accept-Encoding = "deflate";
          } else {
            # unkown algorithm
            remove req.http.Accept-Encoding;
          }
        }

        if (req.url ~ "admin" || req.url ~ "login" || req.url ~ "auth" || req.url ~ "linkcimb" || req.url ~ "resetpass" || req.url ~ "createuser" || req.url ~ "portfolio" || req.url ~ "portfolioitem" || req.url ~ "userprofile" || req.url ~ "userlogin" || req.url ~ "pricealert" || req.url ~ "rosetta" || req.url ~ "cimb" || req.url ~ "iw") {
            return (pass);
        }else{
            return (lookup);
        }
      }

      sub vcl_fetch {
        # static files always cached
        if (req.url ~ "^/static") {
          unset beresp.http.set-cookie;
          return (deliver);
        }

        if (req.url ~ "realtimewithpath" || req.url ~ "performance" || req.url ~ "stockPerformance") {
          set beresp.ttl = 10 s;
        }

        # pass through for anything with a session/csrftoken set
        if (beresp.http.set-cookie ~ "sessionid" || beresp.http.set-cookie ~ "csrftoken") {
          return (hit_for_pass);
        } else {
          return (deliver);
        }

      }

      sub vcl_deliver {
        # multi-server webfarm? set a variable here so you can check
        # the headers to see which frontend served the request
        # set resp.http.X-Server = "server-01";
        if (obj.hits > 0) {
          set resp.http.X-Cache = "HIT";
        } else {
          set resp.http.X-Cache = "MISS";
        }
      }

      sub vcl_hit {
        if (req.request == "PURGE") {
          purge;
          error 200 "OK";
        }
      }

      sub vcl_miss {
        if (req.request == "PURGE") {
          purge;
          error 404 "Not cached";
        }
      }

commands:
  # configure apache
  010_httpd.conf :
    command: "sed -i 's/Listen 8080/Listen 80/g' /etc/httpd/conf/httpd.conf"
  011_httpd.conf :
    command: "sed -i 's/Listen 80/Listen 8080/g' /etc/httpd/conf/httpd.conf"
  012_httpd.conf :
    command: "service httpd restart"
  # configure varnish
  040_varnish:
    command: "sed -i 's/VARNISH_LISTEN_PORT=6081/VARNISH_LISTEN_PORT=80/g' /etc/sysconfig/varnish"
  041_varnish:
    command: "sed -i 's/VARNISH_ADMIN_LISTEN_PORT=6082/VARNISH_ADMIN_LISTEN_PORT=2000/g' /etc/sysconfig/varnish"
  042_varnish:
    command: "service varnish restart"

services:
  sysvinit:
    varnish:
      enabled: true
      ensureRunning: true
